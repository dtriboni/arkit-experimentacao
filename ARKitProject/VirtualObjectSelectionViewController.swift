import UIKit
import SceneKit
import ARKit
import Alamofire
import Zip

class VirtualObjectSelectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	private var tableView: UITableView!
	private var size: CGSize!
    private var marceneiro = [Furniture]()
    weak var delegate: VirtualObjectSelectionViewControllerDelegate?
    final var urlAPI = "https://digitalx-marceneiro-ar-api.azurewebsites.net/"
    
	init(size: CGSize) {
		super.init(nibName: nil, bundle: nil)
		self.size = size
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		tableView = UITableView()
		tableView.frame = CGRect(origin: CGPoint.zero, size: self.size)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.backgroundColor = UIColor.clear
		tableView.separatorEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light))
		tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		tableView.bounces = false

		self.preferredContentSize = self.size
		self.view.addSubview(tableView)

        tableView.isHidden = true
        
        showJSONList()
	}

    
    // Fetch API Furnitures List
    func showJSONList() {
        guard let downloadURL = URL(string: urlAPI + "furniture")  else { return }
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                self.textManager.blurBackground()
                self.textManager.showAlert(title: "Ops... Algo deu errado!",
                                           message: "Nenhum objeto para inserir. Por favor, reinicie o app.")
                print (error as Any)
                return
            }
            do
            {
                let decoder = JSONDecoder()
                let downloadedFurnitures = try decoder.decode([Furniture].self, from: data)
                self.marceneiro = downloadedFurnitures
                DispatchQueue.main.async {
                   self.tableView.reloadData()
                }
            } catch {
                print (error)
                //self.textManager.showAlert(title: "Ops... Algo deu errado!",
                         //                message: "Nenhum objeto para inserir. Por favor, reinicie o app.")
            }
            
        }.resume()
    }
    
    var textManager: TextManager!
    
    // Fetch API current furniture
    func getJSONObject(code: String){
        ///guard let downloadURL = URL(string: "https://libidinous.club/escrivaninha.zip")  else { return }
        //URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
          //  guard let data = data, error == nil, urlResponse != nil else {
            //    print ("ARKIT: JSON Error")
              //  return
           // }
           // print("downloaded with success")
        
            let documentsDirectory = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)

            let escrivaninha = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/mesinha.zip")
            let escrivaninha2 = URL(string: NSHomeDirectory() + "/Documents/mesinha.zip")
    
            //if (FileManager.default.fileExists(atPath: NSHomeDirectory() + "/Documents/mesinha.zip")){
            //   try! FileManager.default.removeItem(atPath: NSHomeDirectory() + "/Documents/mesinha.zip")
            //  print ("ARKIT: \(NSHomeDirectory() + "/Documents/mezinha.zip") removido com sucesso!")
            //}
        
            if let URL = NSURL(string: "https://libidinous.club/mesinha.zip") {
              //  Downloader.load(url: URL as URL, to: escrivaninha)
            }
        
            do
            {
                //let decoder = JSONDecoder()
                //let downloadedFurnitures = try decoder.decode([Furniture].self, from: data)
                //self.marceneiro = downloadedFurnitures
                //self.saveAndLoadObj()
                
                //let filePath = Bundle.main.url(forResource: "file", withExtension: "zip")!
                //let filePath = URL(string: "https://libidinous.club/escrivaninha.zip")
                //let documentsDirectory = FileManager.default.urls(for:.documentDirectory, in: .userDomainMask)[0]
                try Zip.unzipFile(escrivaninha2!, destination: documentsDirectory, overwrite: true, password: "", progress: { (progress) -> () in
                    //self.textManager.showMessage("Baixando objeto: \(progress * 100)%")
                    print("ARKIT: progresso download \(progress * 100) %")
                }) // Unzip
                
                
                let fileManager = FileManager.default
                let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
                do {
                    let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
                    print ("LISTA: \(fileURLs)")
                } catch {
                    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
                }
                
                
                
                loadObject(objPath: NSHomeDirectory() + "/Documents/mesinha.obj")
                
            } catch {
                print("ARKIT: falhou \(error)")
            }
   //     }.resume()
    }
    
    
    func saveAndLoadObj() {
        let objName = marceneiro[0].name
        let strObj = marceneiro[0].obj
        let strMtl = marceneiro[0].mtl
        let textures = marceneiro[0].textures
        
        textures.forEach { object in
            self.importTextures(fileName: object.name, contents: object.content)
        }
        
        self.saveMtlFile(fileName: objName, mtl: strMtl)
        
        self.saveFileToLoad(fileName: objName, obj: strObj)
        
        return
    }
    
    @IBOutlet var sceneView: ARSCNView!
    
    func loadObject(objPath: String) {
        
        print ("ARKIT: carregando cena")
        
        delegate?.virtualObjectSelectionViewController(self, object: loadNode(file: objPath))
        
        print ("ARKIT: cena carregada com sucesso!")
    }
    
    func loadNode(file: String) -> VirtualObject {

        return DynamicObj(objPath: file)
    }
    
    func saveFileToLoad(fileName: String, obj: String) {
        
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("obj")
        
        let fileManager = FileManager.default
        
        //let documents:String = NSHomeDirectory() + "/Documents/\(fileName).obj"
        let documents:String = NSHomeDirectory() + "/Documents/escrivaninha.obj"

        
        if (fileManager.fileExists(atPath: documents)){
            try! fileManager.removeItem(atPath: documents)
            print ("ARKIT: \(documents) removido com sucesso!")
        }
        
        let writeString = obj
        
        do {
            try writeString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            print ("ARKIT: arquivo OBJ salvo com sucesso")
            
        } catch let error as NSError {
            LoadingOverlay.shared.hideOverlayView()
            self.textManager.showAlert(title: "Ops... Algo deu errado!",
                                       message: "Falha ao criar o produto. Tente novamente mais tarde.")
            print("ARKIT: Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
            
        }
        
        loadObject(objPath: documents)
    }
    
    
    func saveMtlFile(fileName: String, mtl: String) {
        
        let fileName = fileName.lowercased()
        
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("mtl")
        
        let fileManager = FileManager.default
        
        let documents:String = NSHomeDirectory() + "/Documents/\(fileName).mtl"
        
        if (fileManager.fileExists(atPath: documents)){
            try! fileManager.removeItem(atPath: documents)
            print ("ARKIT: \(documents) removido com sucesso!")
        }
        
        let writeString = mtl
        
        do {
            try writeString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            print ("ARKIT: arquivo MTL salvo com sucesso")
            
        } catch let error as NSError {
            LoadingOverlay.shared.hideOverlayView()
            self.textManager.showAlert(title: "Ops... Algo deu errado!",
                                       message: "Falha ao criar o produto. Tente novamente mais tarde.")
            print("ARKIT: Failed writing to MTL: \(fileURL), Error: " + error.localizedDescription)
            
        }
    }
    
    
    func importTextures(fileName: String, contents: String) {
        
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(fileName)
        
        let fileManager = FileManager.default
        
        let documents:String = NSHomeDirectory() + "/Documents/\(fileName)"
        
        if (fileManager.fileExists(atPath: documents)){
            try! fileManager.removeItem(atPath: documents)
            print ("ARKIT: \(documents) removido com sucesso!")
        }
        
        let dataDecoded:NSData = NSData(base64Encoded: contents, options: NSData.Base64DecodingOptions(rawValue: 0))!

        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        
        do {
            if let dataImage = UIImageJPEGRepresentation(decodedimage, 1.0) {
                try dataImage.write(to: fileURL)
                print ("ARKIT: arquivo de TEXTURA \(fileName) salvo com sucesso")
            }
            
        } catch let error as NSError {
            LoadingOverlay.shared.hideOverlayView()
            self.textManager.showAlert(title: "Ops... Algo deu errado!",
                                       message: "Falha ao criar o produto. Tente novamente mais tarde.")
            print("ARKIT: Failed writing to MTL: \(fileURL), Error: " + error.localizedDescription)
            
        }
    }
	

	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		//LoadingOverlay.shared.showOverlay(view: self.view)
        getJSONObject(code: marceneiro[indexPath.row].code)
        print ("ARKIT: id linha: \(marceneiro[indexPath.row].code)")
        self.dismiss(animated: true, completion: nil)
	}

	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return marceneiro.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = UITableViewCell()
        
		let label = UILabel(frame: CGRect(x: 53, y: 10, width: 200, height: 30))
		let icon = UIImageView(frame: CGRect(x: 15, y: 10, width: 30, height: 30))

		cell.backgroundColor = UIColor.clear
		cell.selectionStyle = .none
		let vibrancyEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .extraLight))
		let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
		vibrancyView.frame = cell.contentView.frame
		cell.contentView.insertSubview(vibrancyView, at: 0)
		vibrancyView.contentView.addSubview(label)
		vibrancyView.contentView.addSubview(icon)

		// Fill up the cell with data from the object.
		//let object = getObject(index: indexPath.row)
		//var thumbnailImage = object.thumbImage!
		//if let invertedImage = thumbnailImage.inverted() {
			//thumbnailImage = invertedImage
		//}
		//label.text = object.title
        label.text = marceneiro[indexPath.row].name
		//icon.image = thumbnailImage
        tableView.isHidden = false
        
		return cell
	}

	func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath)
		cell?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
	}

	func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath)
		cell?.backgroundColor = UIColor.clear
	}
}

// MARK: - VirtualObjectSelectionViewControllerDelegate
protocol VirtualObjectSelectionViewControllerDelegate: class {
	func virtualObjectSelectionViewController(_: VirtualObjectSelectionViewController, object: VirtualObject)
}
