import Foundation

class Estante: VirtualObject {
    
    override init() {
        super.init(modelName: "estante", fileExtension: "scn", thumbImageFilename: "candle", title: "Comoda")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

