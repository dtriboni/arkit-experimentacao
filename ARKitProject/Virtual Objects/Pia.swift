//
//  Pia.swift
//  ARKitProject
//
//  Created by Daniel Triboni on 28/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//


import Foundation

class Pia: VirtualObject {
    
    override init() {
        super.init(modelName: "pia", fileExtension: "scn", thumbImageFilename: "lamp", title: "Pia")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




