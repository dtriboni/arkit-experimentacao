//
//  DynamicObj.swift
//  ARKitProject - Import Objects Dynamically by API
//
//  Created by Daniel Triboni on 04/04/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class DynamicObj: VirtualObject {
    
    let objPath: String
    
    init(objPath: String) {
        
        self.objPath = objPath
        
        super.init(modelName: "", fileExtension: objPath, thumbImageFilename: "chair", title: "Collada")
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
