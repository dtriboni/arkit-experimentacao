//
//  luminaria.swift
//  ARKitProject
//
//  Created by Daniel Triboni on 28/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class Luminaria: VirtualObject {
    
    override init() {
        super.init(modelName: "estante", fileExtension: "scn", thumbImageFilename: "candle", title: "Estante")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


