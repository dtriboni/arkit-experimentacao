//
//  bacia.swift
//  ARKitProject
//
//  Created by Daniel Triboni on 28/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//


import Foundation

class Bacia: VirtualObject {
    
    override init() {
        super.init(modelName: "bacia", fileExtension: "scn", thumbImageFilename: "lamp", title: "Bacia")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



