import Foundation
import ARKit

extension ARCamera.TrackingState {
	var presentationString: String {
		switch self {
        case .notAvailable:
            return "Reconhecimento de superfície\nnão disponível!"
        case .normal:
            return "Reconhecendo superfície"
        case .limited(let reason):
            switch reason {
            case .excessiveMotion:
                return "Sem reconhecimento\nMova a câmera suavemente"
            case .insufficientFeatures:
                return "Sem reconhecimento\nSem detalhes da superfície"
            case .initializing:
                return "Inicializando"
            case .relocalizing:
                return "Reiniciando..."
            }
        }
	}
}
