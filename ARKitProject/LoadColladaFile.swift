//
//  LoadColladaFile.swift
//  ARKitProject - Load Downloaded Collada File to current Scenario
//
//  Created by Daniel Triboni on 02/04/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ARKit
import SceneKit

class LoadColladaFile {
    
    @IBOutlet var sceneView: ARSCNView!
    
    func loadObject(fileName: String) {
        
        let groupNode = SCNNode()
        
        groupNode.addChildNode(loadNode(file: fileName,
                                        loc: SCNVector3(x: -1.0, y:-1.4, z:-4),
                                        scale: SCNVector3(x: 0.07, y:0.07, z:0.07)))
        
        sceneView.scene.rootNode.addChildNode(groupNode)
    }
    
    func loadNode(file: String, loc:SCNVector3, scale: SCNVector3) -> SCNNode {
        let loadingObjNode = SCNNode()
        let loadingScene = SCNScene(named: file)!
        let nodeArray = loadingScene.rootNode.childNodes
        
        loadingObjNode.position = loc
        loadingObjNode.scale = scale
        
        for childNode in nodeArray {
            loadingObjNode.addChildNode(childNode as SCNNode)
        }
        
        return loadingObjNode
    }
}
