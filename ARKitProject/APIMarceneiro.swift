//
//  APIMarceneiro.swift
//  ARKitProject - Calling API from converted Babylon objects
//
//  Created by Daniel Triboni on 23/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

public struct Furnitures: Codable {
    public struct Furniture: Codable {
        public struct Textures: Codable {
            public let name: String
            public let content: String
        }
        
        public let name: String
        public let code: String
        public let _id: String
        public let obj: String
        public let mtl: String
        public let textures: [Textures]
        
        private enum CodingKeys: String, CodingKey {
            case name
            case code
            case _id
            case obj
            case mtl
            case textures = "texture"
        }
    }
    
    public let furniture: Furniture
}

public typealias Furniture = Furnitures.Furniture
public typealias Texture = Furniture.Textures

extension Texture {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        content = try container.decode(String.self, forKey: .content)
    }
}

