//
//  Downloader.swift
//  ARKitProject
//
//  Created by Daniel Triboni on 16/04/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class Downloader {
    class func load(url: URL, to localUrl: URL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = try! URLRequest(url: url, method: .get)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("ARKIT: Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    //completion()
                     print("ARKIT: Success to copy item")
                } catch (let writeError) {
                    print("ARKIT: error writing file \(localUrl) : \(writeError)")
                }
                
            } else {
                print("ARKIT: Failure: %@", error?.localizedDescription ?? "");
            }
        }
        task.resume()
    }
}

